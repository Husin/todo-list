package com.brontoproject.todolist_amdroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "todolist";
    private static final int DB_VER = 1;
    private static final String DB_TABLE = "todo";
    private static final String DB_COLUMN ="nama_todo";
    private static final String DB_DATE = "date_todo";
    private static final String DB_TIME = "time_todo";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, nama_todo TEXT NOT NULL, date_todo DATE NOT NULL, time_todo TIME NOT NULL);",DB_TABLE,DB_COLUMN);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DELETE TABLE IF EXISTS %s",DB_TABLE);
        db.execSQL(query);
        onCreate(db);
    }

    public void addTodo(String task, String dateValue, String timeValue){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_COLUMN,task);
        values.put(DB_DATE, dateValue);
        values.put(DB_TIME, timeValue);
//      db.insertWithOnConflict(DB_TABLE, null, values,SQLiteDatabase.CONFLICT_REPLACE);
        db.insert(DB_TABLE, null,values);
        db.close();
    }

//update harus make 2 parameter, Data sebelum di edit ditaro di whereArgs
    public void upTodo(String beforeTask,String newTask, String dateValue, String timeValue){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_COLUMN, newTask);
        values.put(DB_DATE, dateValue);
        values.put(DB_TIME, timeValue);
        db.update(DB_TABLE, values, DB_COLUMN + " = ?", new String[]{beforeTask});
//        db.replace(DB_TABLE,null, values);
        Log.d("update data", String.valueOf(values));
        db.close();


    }

    public void deltTodo(String task){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE,DB_COLUMN + " = ?", new String[]{task});
        Log.d("delete data", task);
        db.close();

    }

    public  ArrayList<String> showTask(){
        ArrayList<String> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DB_TABLE,new String[]{DB_COLUMN},null,null,
        null,null,null);
        while(cursor.moveToNext()){
            int index = cursor.getColumnIndex(DB_COLUMN);
            list.add(cursor.getString(index));
          }
        cursor.close();
        db.close();
        return list;
    }

    public  ArrayList<String> showDateTime(){
        ArrayList<String> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DB_TABLE,new String[]{DB_DATE, DB_TIME},null,null,
                null,null,null);
        while(cursor.moveToNext()){
            int index1 = cursor.getColumnIndex(DB_DATE);
            int index2 = cursor.getColumnIndex(DB_TIME);
                        String data = cursor.getString(index1) + " | " + cursor.getString(index2);
            list.add(data);
        }
        cursor.close();
        db.close();
        return list;
    }
}
